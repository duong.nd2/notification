/* eslint-disable  */
import { register } from "register-service-worker";

if (process.env.NODE_ENV === "development") {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready(e) {
      console.log(e,"[--------------------]");
      console.log(
        "App is being served from cache by a service worker.\n" +
          "For more details, visit https://goo.gl/AFskqB"
      );
       
    },
    registered(m) {
      console.log("Service worker has been registered. duongggg");
      // Notification.requestPermission(function(result) {
      //   if (result === 'granted') {
      //     setInterval(() => {
      //       console.log('a');
      //       navigator.serviceWorker.ready.then(function(registration) {
      //         registration.showNotification('Vibration Sample', {
      //           body: 'Buzz! Buzz!',
      //           icon: '',
      //           vibrate: [200, 100, 200, 100, 200, 100, 200],
      //           tag: 'vibration-sample'
      //         });
      //       });
      //     }, );
      //   }
      // });
    },
    cached() {
      console.log("Content has been cached for offline use.");
    },
    updatefound() {
      console.log("New content is downloading.");
    },
    updated() {
      console.log("New content is available; please refresh.");
    },
    offline() {
      console.log(
        "No internet connection found. App is running in offline mode."
      );
    },
    error(error) {
      console.error("Error during service worker registration:", error);
    },
  });
}
